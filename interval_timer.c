#include <syslog.h>
#include "interval_timer.h"

static int set_sigset(sigset_t *set, int signo)
{
	if (sigemptyset(set) != 0) {
		syslog(LOG_ERR, "sigemptyset failed: %m");
		return -1;
	}
	if (sigaddset(set, signo) != 0) {
		syslog(LOG_ERR, "sigaddset of RT signal %d failed: %m", signo);
		return -1;
	}
	return 0;
}

int timer_open(struct interval_timer *new_timer, int usecs)
{
	// This contains the RT signal to use for the next timer created.  I
	// don't bother recycling these, so once you run out, that's it!  Zero
	// means uninitialized, apparently SIGRTMIN is a macro that calls a
	// function, who knew?
	static int next_rt_signal = 0;

	if (next_rt_signal == 0) {
		next_rt_signal = SIGRTMIN;
	}

	// Pick an RT signal to use (this is very not thread safe!)
	new_timer->rt_signal = next_rt_signal++;
	if (new_timer->rt_signal > SIGRTMAX) {
		syslog(LOG_ERR, "ran out of RT signals");
		return -1;
	}

	// Block the signal, so we don't kill the process
	sigset_t set;
	if (set_sigset(&set, new_timer->rt_signal) != 0) {
		return -1;
	}
	if (sigprocmask(SIG_BLOCK, &set, NULL) != 0) {
		syslog(LOG_ERR, "failed to mask RT signal %d: %m",
				new_timer->rt_signal);
		return -1;
	}

	// OK, create us a timer
	struct sigevent sigev = {
		.sigev_notify = SIGEV_SIGNAL,
		.sigev_signo = new_timer->rt_signal,
	};
	if (timer_create(CLOCK_MONOTONIC, &sigev, &new_timer->timer_id) != 0) {
		syslog(LOG_ERR, "failed to create a timer: %m");
		return -1;
	}

	int result = timer_reset(new_timer, usecs);
	if (result != 0) {
		timer_close(new_timer);
		return result;
	}
	return 0;
}

int timer_reset(struct interval_timer *timer, int usecs)
{
	// Arm the timer
	struct itimerspec timerspec = {
		.it_interval = {
			.tv_sec = usecs / 1000000,
			.tv_nsec = 1000 * (usecs % 1000000),
		},
		.it_value = {
			.tv_sec = usecs / 1000000,
			.tv_nsec = 1000 * (usecs % 1000000),
		},
	};
	if (timer_settime(timer->timer_id, 0, &timerspec, NULL) != 0) {
		syslog(LOG_ERR, "failed to set timer: %m");
		return -1;
	}
	return 0;
}

int timer_wait(struct interval_timer *timer)
{
	sigset_t set;
	if (set_sigset(&set, timer->rt_signal) != 0) {
		return -1;
	}
	if (sigwaitinfo(&set, NULL) == -1) {
		syslog(LOG_WARNING, "sigwaitinfo failed: %m");
		return -1;
	}
	return 0;
}

void timer_close(struct interval_timer *timer)
{
	// Note that I only bother to destroy the timer, the signal is still
	// used up and blocked in the process signal mask
	if (timer_delete(timer->timer_id) != 0) {
		syslog(LOG_WARNING, "failed to destroy a timer: %m");
	}
}
