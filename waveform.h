#ifndef WAVEFORM_H
#define WAVEFORM_H

#include <pulse/simple.h>
#include <pulse/error.h>

#define SAMPLE_BUFF_LEN 20000U

struct waveform {
	pa_simple *pa_handle;
	double *sine_wave;
	size_t sine_wave_len;
	size_t sine_wave_pos;
	double *ramp_up;
	size_t ramp_up_len;
	size_t ramp_up_pos;
	float sample_buffer[SAMPLE_BUFF_LEN];
	size_t sample_pos;
};

// Create a tone generator for the given Hz and edge length in milliseconds
int waveform_new(struct waveform *self, int tone_hz, int edge_ms);
int waveform_keydown(struct waveform *self, int duration_us);
int waveform_keyup(struct waveform *self, int duration_us);
void waveform_destroy(struct waveform *self);

#endif
