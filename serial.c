#include <unistd.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <termios.h>
#include <stdlib.h>
#include <syslog.h>
#include "serial.h"

int serial_open(struct port *new_port, const char *port)
{
	new_port->fd = open(port, O_RDWR | O_NOCTTY);
	if (new_port->fd == -1) {
		syslog(LOG_ERR, "failed to open %s: %m", port);
		return -1;
	}
	// We must enable DTR so we get power to the paddle
	int bits = TIOCM_DTR;
	if (ioctl(new_port->fd, TIOCMSET, &bits) == -1) {
		syslog(LOG_ERR, "failed to enable DTR on %s: %m", port);
		serial_close(new_port);
		return -1;
	}

	return 0;
}

int serial_get_bits(struct port *pt)
{
	int system_bits = 0;
	int bits = 0;
	if (ioctl(pt->fd, TIOCMGET, &system_bits) == -1) {
		syslog(LOG_ERR, "failed to read modem bits: %m");
		return -1;
	}
	if ((system_bits & TIOCM_DSR) != 0)
		bits |= DSR_BIT;
	if ((system_bits & TIOCM_CTS) != 0)
		bits |= CTS_BIT;
	return bits;
}

void serial_close(struct port *pt)
{
	close(pt->fd);
	pt->fd = -1;
}

