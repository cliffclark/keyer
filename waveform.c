#include <stdlib.h>
#include <math.h>
#include <syslog.h>
#include "waveform.h"

static const int64_t sample_rate = 44100;

int waveform_new(struct waveform *self, int tone_hz, int edge_ms)
{
	pa_sample_spec ss = {
		.format = PA_SAMPLE_FLOAT32NE,
		.rate = sample_rate,
		.channels = 1,
	};

	pa_buffer_attr buff_attr = {
		.maxlength = ~0,
		.tlength = pa_usec_to_bytes(20000, &ss),
		.prebuf = ~0,
		.minreq = ~0,
		.fragsize = ~0,
	};

	int error;
	self->pa_handle = pa_simple_new(
			NULL, // Default server
			"Keyer", // App name
			PA_STREAM_PLAYBACK,
			NULL, // Default device
			"CW", // Description
			&ss,
			NULL, // Default channel map
			&buff_attr,
			&error // Ignore errors
	);
	if (self->pa_handle == NULL) {
		syslog(LOG_ERR, "Failed to open pa: %s", pa_strerror(error));
		return -1;
	}

	// Allocate one cycle of the sine wave
	self->sine_wave_len = (sample_rate + tone_hz - 1) / tone_hz;
	self->sine_wave =
		malloc(self->sine_wave_len * sizeof(self->sine_wave[0]));
	if (self->sine_wave == NULL) {
		syslog(LOG_ERR, "Failed to allocate memory for sine wave");
		waveform_destroy(self);
		return -1;
	}

	// Calculate one cycle of the sine wave
	for (size_t i = 0; i < self->sine_wave_len; ++i) {
		self->sine_wave[i] =
			0.9 * sin(2 * M_PI * i / self->sine_wave_len);
	}

	// Allocate factors for shaping the signal edges
	self->ramp_up_len = (sample_rate * edge_ms + 999) / 1000;
	self->ramp_up = malloc(self->ramp_up_len * sizeof(self->ramp_up[0]));
	if (self->ramp_up == NULL) {
		syslog(LOG_ERR, "Failed to allocate memory for signal shape");
		waveform_destroy(self);
		return -1;
	}

	// Compute the ramp-up of the edges
	for (size_t i = 0; i < self->ramp_up_len; ++i) {
		self->ramp_up[i] =
				0.5 * (1. - cos(M_PI * i / self->ramp_up_len));
	}

	// Initialize our current positions
	self->sine_wave_pos = 0;
	self->ramp_up_pos = 0;
	self->sample_pos = 0;

	return 0;
}

static size_t samples_to_write(int duration_us)
{
	// Compute number of samples
	size_t result = (sample_rate * duration_us + 999999) / 1000000;
	return result;
}

static double next_sine(struct waveform *self)
{
	double sample = self->sine_wave[self->sine_wave_pos];
	self->sine_wave_pos += 1;
	self->sine_wave_pos %= self->sine_wave_len;

	return sample;
}

static int write_to_pa(struct waveform *self, float *samples, size_t count)
{
	// Write it out
	int error;
	if (pa_simple_write(self->pa_handle, samples,
			count * sizeof(*samples), &error) == -1) {
		syslog(LOG_ERR, "Failed to write pa: %s", pa_strerror(error));
		return -1;
	}

	return 0;
}

static int flush_buffer(struct waveform *self)
{
	// Flush the buffer
	if (self->sample_pos > 0) {
		int rc = write_to_pa(self, self->sample_buffer,
				self->sample_pos);
		if (rc != 0)
			return rc;
		self->sample_pos = 0;
	}

	return 0;
}

static int write_sample(struct waveform *self, double sample)
{
	if (self->sample_pos == SAMPLE_BUFF_LEN) {
		if (flush_buffer(self) == -1)
			return -1;
	}

	self->sample_buffer[self->sample_pos] = sample;
	self->sample_pos += 1;

	return 0;
}

int waveform_keydown(struct waveform *self, int duration_us)
{
	// Compute number of samples
	size_t count = samples_to_write(duration_us);
	
	// Prepare the data to send
	for (size_t i = 0; i < count; ++i) {
		double sample = next_sine(self);
		// Ramp down the rising edge of the signal
		if (self->ramp_up_pos < self->ramp_up_len) {
			sample *= self->ramp_up[self->ramp_up_pos];
			self->ramp_up_pos += 1;
		}
		if (write_sample(self, sample) != 0)
			return -1;
	}

	return flush_buffer(self);
}

int waveform_keyup(struct waveform *self, int duration_us)
{
	// Compute the number of samples
	size_t count = samples_to_write(duration_us);

	// Prepare falling edge or silence
	for (size_t i = 0; i < count; ++i) {
		double sample = 0;
		// Only decrement the postion if it is more than the beginning
		// we need to decrement before using it because it can point
		// past the end of the array.
		if (self->ramp_up_pos > 0) {
			self->ramp_up_pos -= 1;
			sample = next_sine(self);
			sample *= self->ramp_up[self->ramp_up_pos];
		}
		if (write_sample(self, sample) != 0)
			return -1;
	}

	// Start the next tone at zero
	if (self->ramp_up_pos == 0) {
		self->sine_wave_pos = 0;
	}

	return flush_buffer(self);
}

void waveform_destroy(struct waveform *self)
{
	if (self->pa_handle != NULL) {
		pa_simple_free(self->pa_handle);
	}
	if (self->sine_wave != NULL) {
		free(self->sine_wave);
	}
	if (self->ramp_up != NULL) {
		free(self->ramp_up);
	}
}
