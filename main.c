#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <getopt.h>
#include "serial.h"
#include "interval_timer.h"
#include "waveform.h"

enum mode {
	HAND_KEY,
	IAMBIC_A,
};

enum element {
	DIT,
	DAH,
};

struct {
	char *port;
	int hz;
	int mode;
	int wpm;
} config = {
	.port = "/dev/ttyS0",
	.hz = 650,
	.mode = HAND_KEY,
	.wpm = 12,
};

// Constants
static const int poll_interval = 5000;

static void usage(void)
{
	fprintf(stderr, "usage: keyer [options]\n"
		"Options:\n"
		"-p, --port <port>: Set serial port to use for input\n"
		"-f, --hz <freq>: Set tone freqency\n"
		"-w, --wpm <wpm>: Set wpm for automatic keying\n"
		"-h, --help: Display this help message\n");

}

static void hand_key(struct port *pt, struct waveform *wf)
{
	// Set up our timer for 5 ms
	struct interval_timer poll_timer;
	if (timer_open(&poll_timer, poll_interval) != 0)
		exit(EXIT_FAILURE);

	for (;;) {
		int new_bits = serial_get_bits(pt);
		if (new_bits == -1)
			exit(EXIT_FAILURE);
		if (new_bits)
			waveform_keydown(wf, poll_interval);
		else
			waveform_keyup(wf, poll_interval);
		timer_wait(&poll_timer);
	}
}

static void dit(struct interval_timer *timer, struct waveform *wf,
		int dit_period)
{
	// Reset the timer so we can wait for the element
	if (timer_reset(timer, dit_period*2) != 0)
		exit(EXIT_FAILURE);

	// Key down for one dit period, then key up for a dit
	waveform_keydown(wf, dit_period);
	waveform_keyup(wf, dit_period);

	// Wait for the element to be over
	if (timer_wait(timer) != 0)
		exit(EXIT_FAILURE);
}

static void dah(struct interval_timer *timer, struct waveform *wf,
		int dit_period)
{
	// Reset the timer so we can wait for the element
	if (timer_reset(timer, dit_period*4) != 0)
		exit(EXIT_FAILURE);

	// Key down for the dah, then key up for a dit
	waveform_keydown(wf, dit_period*3);
	waveform_keyup(wf, dit_period);
	if (timer_wait(timer) != 0)
		exit(EXIT_FAILURE);
}

static void iambic_a(struct port *pt, struct waveform *wf)
{
	// Compute the length of a dit in microseconds
	const int dit_period = 1200000 / config.wpm;

	// Create a timer
	struct interval_timer timer;
	if (timer_open(&timer, poll_interval) != 0)
		exit(EXIT_FAILURE);

	int last_element = DIT;
	for (;;) {
		// Idle until we start the next character
		// Reset the timer for the poll interval, and wait for
		// something to happen
		if (timer_reset(&timer, poll_interval) != 0)
			exit(EXIT_FAILURE);
		int new_bits;
		do {
			new_bits = serial_get_bits(pt);
		} while (new_bits == 0);
		if (new_bits == -1)
			exit(EXIT_FAILURE);

		if (new_bits == DSR_BIT) {
			// DIT
			dit(&timer, wf, dit_period);
			last_element = DIT;
		} else if (new_bits == CTS_BIT) {
			// DAH
			dah(&timer, wf, dit_period);
			last_element = DAH;
		} else if (new_bits == (CTS_BIT | DSR_BIT)) {
			// Iambic (mode A)
			// complete the alternate character
			if (last_element == DAH) {
				// DIT
				dit(&timer, wf, dit_period);
				last_element = DIT;
			} else {
				// DAH
				dah(&timer, wf, dit_period);
				last_element = DAH;
			}
		}
	}
}

int main(int argc, char **argv)
{
	openlog("keyer", LOG_PERROR | LOG_NDELAY, LOG_USER);

	// Command line options
	struct option options[] = {
		{ "port", required_argument, NULL, 'p' },
		{ "hz", required_argument, NULL, 'f' },
		{ "wpm", required_argument, NULL, 'w' },
		{ "help", no_argument, NULL, 'h' },
		{ NULL, 0, NULL, 0 }
	};

	for (;;) {
		int index = 0;
		int c = getopt_long(argc, argv, "p:f:w:h", options, &index);
		if (c == -1)
			break;
		switch (c) {
		case 'p':
			config.port = optarg;
			break;
		case 'f':
			config.hz = atoi(optarg);
			break;
		case 'w':
			config.mode = IAMBIC_A;
			config.wpm = atoi(optarg);
			break;
		case 'h':
			usage();
			exit(EXIT_SUCCESS);
		default:
			usage();
			exit(EXIT_FAILURE);
		}
	}

	if (optind < argc) {
		usage();
		exit(EXIT_FAILURE);
	}

	struct port pt;
	if (serial_open(&pt, config.port) == -1) {
		exit(EXIT_FAILURE);
	}

	struct waveform wf;
	if (waveform_new(&wf, config.hz, 4) != 0) {
		exit(EXIT_FAILURE);
	}

	if (config.mode == HAND_KEY)
		hand_key(&pt, &wf);
	else
		iambic_a(&pt, &wf);

	serial_close(&pt);

	return 0;
}
