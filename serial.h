#ifndef SERIAL_H
#define SERIAL_H

struct port {
	int fd;
};

enum {
	DSR_BIT = 1,
	CTS_BIT = 2
};

int serial_open(struct port *new_port, const char *port);
int serial_get_bits(struct port *pt);
void serial_close(struct port *pt);

#endif
