#ifndef INTERVAL_TIMER_H
#define INTERVAL_TIMER_H

#include <signal.h>
#include <time.h>

struct interval_timer {
	int rt_signal;
	timer_t timer_id;
};

// Watch it! these functions are not thread safe
int timer_open(struct interval_timer *new_timer, int usecs);
int timer_reset(struct interval_timer *timer, int usecs);
int timer_wait(struct interval_timer *timer);
void timer_close(struct interval_timer *timer);

#endif
